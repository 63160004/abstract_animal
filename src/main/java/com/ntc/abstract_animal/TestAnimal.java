/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.abstract_animal;

/**
 *
 * @author L4ZY
 */
public class TestAnimal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Human h1 = new Human("Boy");
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        h1.run();
        System.out.println("================================");

        Cat c1 = new Cat("Aimeow");
        c1.eat();
        c1.walk();
        c1.speak();
        c1.sleep();
        c1.run();
        System.out.println("================================");

        Dog d1 = new Dog("Dummy");
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        d1.run();
        System.out.println("================================");

        Bat b1 = new Bat("kangkaw");
        b1.eat();
        b1.speak();
        b1.sleep();
        b1.fly();
        System.out.println("================================");

        Bird b2 = new Bird("kangkaw");
        b2.eat();
        b2.speak();
        b2.sleep();
        b2.fly();
        System.out.println("================================");

        Fish f1 = new Fish("dook");
        f1.eat();
        f1.speak();
        f1.swim();
        f1.sleep();
        System.out.println("================================");

        Crab cb1 = new Crab("ying");
        cb1.eat();
        cb1.speak();
        cb1.swim();
        cb1.sleep();
        System.out.println("================================");

        Crocodile croc1 = new Crocodile("Chalawan");
        croc1.eat();
        croc1.speak();
        croc1.crawl();
        System.out.println("================================");

        Snake s1 = new Snake("Chongarng");
        s1.eat();
        s1.speak();
        s1.crawl();
        System.out.println("================================");

    }

}
