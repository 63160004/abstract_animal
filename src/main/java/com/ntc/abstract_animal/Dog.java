/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.abstract_animal;

/**
 *
 * @author L4ZY
 */
public class Dog extends LandAnimal {

    private String nickname;

    public Dog(String nickname) {
        super("Dog", 4);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Dog " + nickname + " can run");
    }

    @Override
    public void walk() {
        System.out.println("Dog " + nickname + " can walk");
    }

    @Override
    public void speak() {
        System.out.println("Dog " + nickname + " can speak");
    }

    @Override
    public void eat() {
        System.out.println("Dog " + nickname + " can eat");
    }

    @Override
    public void sleep() {
        System.out.println("Dog " + nickname + " can sleep");
    }

}
