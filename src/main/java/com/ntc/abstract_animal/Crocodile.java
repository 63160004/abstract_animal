/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.abstract_animal;

/**
 *
 * @author L4ZY
 */
public class Crocodile extends Reptile {

    private String nickname;

    public Crocodile(String nickname) {
        super("Crocodile", 4);
        this.nickname = nickname;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile: " + nickname + " can crawl");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile: " + nickname + " can walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile: " + nickname + " can speak");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile: " + nickname + " can eat");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile: " + nickname + " can sleep");
    }

}
