/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.abstract_animal;

/**
 *
 * @author L4ZY
 */
public abstract class AquaticAnimal extends Animal {

    public AquaticAnimal(String name, int numOfLeg) {
        super(name, 0);
    }

    public abstract void swim();

}
