/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.abstract_animal;

/**
 *
 * @author L4ZY
 */
public class Car extends Vehicle implements Runable {

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Car startEngine");   
    }

    @Override
    public void stopEngine() {
      System.out.println("Car stopEngine");   
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car raiseSpeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car applyBreak");
    }

    @Override
    public void Run() {
        System.out.println("Car can run");
    }

}
