/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.abstract_animal;

/**
 *
 * @author L4ZY
 */
public abstract class LandAnimal extends Animal implements Runable{

    public LandAnimal(String name, int numOfLeg) {
        super(name, numOfLeg);
    }

    public abstract void run();

}
