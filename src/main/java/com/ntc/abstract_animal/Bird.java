/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.abstract_animal;

/**
 *
 * @author L4ZY
 */
public class Bird extends Poultry {

    private String nickname;

    public Bird(String nickname) {
        super("Bird", 2);
        this.nickname = nickname;
    }

    @Override
    public void fly() {
        System.out.println("Bird " + nickname + " can fly");
    }

    @Override
    public void walk() {
        System.out.println("Bird " + nickname + " can walk");
    }

    @Override
    public void speak() {
        System.out.println("Bird " + nickname + " can speak");
    }

    @Override
    public void eat() {
        System.out.println("Bird " + nickname + " can eat");
    }

    @Override
    public void sleep() {
        System.out.println("Bird " + nickname + " can sleep");
    }

}
